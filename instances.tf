resource "aws_instance" "frontend" {
  count             = "${var.nfe}"

  ami               = "ami-04681a1dbd79675a5"
  instance_type     = "t2.micro"

  #availability_zone = "${data.aws_availability_zones.available.names[count.index % var.max_az]}"
  availability_zone = "${element(aws_subnet.main.*.availability_zone, count.index)}"
  #subnet_id         = "${aws_subnet.main.*.id[count.index % var.max_az]}"
  subnet_id         = "${element(aws_subnet.main.*.id, count.index)}"

  key_name          = "${var.project}"

  tags {
    Name      = "${var.project}-fe${count.index+1}"
    Project   = "${var.project}"
  }

  vpc_security_group_ids  = ["${aws_security_group.fe_group.id}"]
}
