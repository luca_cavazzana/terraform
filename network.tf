resource "aws_vpc" "main" {
  cidr_block  = "10.0.0.0/16"

  tags = {
    Name      = "${var.project}"
    Project   = "${var.project}"
  }
}


resource "aws_subnet" "main" {
  count = "${floor(min(var.max_az, var.nfe)+0.5)}"

  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "10.0.${16*count.index}.0/20"

  availability_zone = "${element(data.aws_availability_zones.available.names,count.index)}"

  tags = {
    Name      = "${var.project}_${count.index}"
    Project   = "${var.project}"
  }
}
