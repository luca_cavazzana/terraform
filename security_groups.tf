resource "aws_security_group" "fe_group" {
  description   = "SG for ${var.project} frontends"

  name          = "${var.project}_fe"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name      = "${var.project}"
    Project   = "${var.project}"
  }
}
