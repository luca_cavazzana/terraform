provider "aws" {
  profile   = "terraform"
  region    = "us-east-1"

  version   = "~> 1.35"
}
