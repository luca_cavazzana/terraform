variable "project" {
  description = "Project name, will pop everywhere"
  #default     = "asd"
}

variable "nfe" {
  description = "Number of FE to deploy"
  #default     = 2
}

variable "max_az" {
  description = "Max number of AZs"
  default     = 3
}
